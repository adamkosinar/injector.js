

module.exports = {
    "_REQUIRE_ROOT" : { type: "value", implementation: __dirname },
    "_EXTERNAL" : {type: "value", implementation: [ require("onscroll-reporting-clients"), require("onscroll-ad-transaction-store") ]},
    "config" : { type: "value", require: "./config" },
    "core" : { type: "type", require: "./src/Core/Core" },
    "dataProviderFactory" : { type: "type", require: "./src/DataProviderFactory/DataProviderFactory" },
    "AbstractDataProvider" : { type: "value", require: "./src/DataProvider/AbstractDataProvider" },
    "Cron" : { type: "value", require: "cron" }
};

