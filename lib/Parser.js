var _ = require("lodash");

function Parser() {


}


Parser.prototype.parseArguments = function (fn) {

    if (!_.isFunction(fn)) {
        throw  new Error("Argument is not a function! " + fn +" given");
    }

    return _.filter(fn.toString().match(/^function\s*[^\(]*\(\s*([^\)]*)\)/m), function (val) {
        return (val.match(/[^,\s][^\,]*[^,\s]*/) && val.indexOf("function") === -1);
    })[0]
        .split(",")
        .map(function (arg) {
            return arg.trim();
        });
};

Parser.prototype.parseAnnotation = function (fn) {

    if (!_.isFunction(fn)) {
        throw  new Error("Argument is not a function! " + fn +" given");
    }

    var annotation =_.filter(fn.toString().match(/((?:\/\*(?:[^*]|(?:\*+[^*\/]))*\*+\/)|(?:\/\/.*))/), function (matched) {
        return (matched.indexOf("@annotate") !== -1);
    });

    if (_.isEmpty(annotation)) return [];

    // TODO: do not be lazy and find better solution!
    return annotation[0]
        .replace(/(\/\/)||(\/\*)||(\*\/)||(\*)||\s/, "")
        .replace("@annotate ", "").split(",").map(function(arg) {
            return arg.trim();
        });

};


module.exports = Parser;
