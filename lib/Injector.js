var _ = require("lodash");

function Injector(modules, parser) {

    this.parser = parser;
    this.modules = modules;
}


Injector.prototype.invoke = function (fn) {

    var args = this.parser.parseArguments(fn),
        annotated = this.parser.parseAnnotation(fn);

    args = overrideWithAnnotated(args, annotated);

};


function overrideWithAnnotated(args, annotated) {

    _.each(annotated, function (val, index) {
        args[index] = val;
    });

    return args;
}



module.exports = Injector;
